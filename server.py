#!/usr/bin/env python3
import time
from flask import Flask, json, request, jsonify
app = Flask(__name__)

from lib.Utilities import *

@app.route('/pin/out/<number>/<action>')
def do_pin_output(number,action):
    '''
     generic http interface for turn a pin on and off
    '''
    status = pin_output(number,action)
    return jsonify(status)

@app.route('/pin/in/<number>/<callback>')
def do_pin_input(number,callback):
    '''
    generic http interface for setting up pin input
    only really useful if you know the name of the function 
    in this project you want to set up
    '''
    status = pin_input(number,callback)
    return jsonify(status)

@app.route('/pin/in/cbwipe/<number>')
def do_pin_cb_removal(number):
    '''
    http interface for GPIO.cleanup
    '''
    status = pin_callback_removal(number)
    return jsonify(status)

@app.route('/feed/<size>')
def do_feed(size):
    '''
    http interface for initiating the feeding routine
    '''
    status = feeding(size)
    return jsonify(status)


@app.route('/pin/counter')
def do_inc_test():
    return str(get_counter())

@app.route('/pin/toggle/<number>')
def do_toggle(number):
    status = pin_toggle(number)
    return jsonify(status)

@app.route('/camera/pic')
def do_take_pic():
    filename = take_pic()
    status = {"filename" : filename }
    return jsonify(status)

if __name__ == "__main__":
    print dechooch_motors()
    app.run(debug=True)
