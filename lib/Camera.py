from Config import PICS_DIR
import time
from picamera import PiCamera

def take_pic(res_x = 1920 , res_y = 1080):
    camera = PiCamera()
    camera.resolution = (res_x , res_y)
    camera.start_preview()
    time.sleep(1)
    filename = "%s/%s.%s" % (PICS_DIR, time.strftime("%M-%d-%Y_%H.%S"),"jpeg")
    camera.capture(filename)
    camera.stop_preview()
    camera.close()

