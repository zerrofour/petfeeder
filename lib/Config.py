PICS_DIR="/home/pi"

'''
Feeder specific pin and motor assignments
Each assignment is a dict of
 {
      pin_number: int(n),
      desc: "ting that does a thing",
      pin_state_chooch: on | off,
      pin_state_dechooch: on | off
 }
due to the quirks of relays, sometimes when a pin is 'on', it turns the relay off and vice versa. 
setting this here should make other stuff more intuitive to use, that is to say, what state must
the pin be in to make the thing attached to it do the thing? figure it out once and then just use 
FOO["pin_state_chooch"] to make it chooch

'''

BARREL_MOTOR = { "pin_number" : 36, "desc": "motor that turns the feeding barrel", "pin_state_chooch": "on", "pin_state_dechooch": "off" }
VIBRATION_MOTOR = { "pin_number" : 35, "desc":"vibration motor for shuffling food pellets",  "pin_state_chooch": "on", "pin_state_dechooch": "off" }
ROTATION_SENSOR = { "pin_number" : 38, "desc" : "switch that is closed during barrel rotation cycle",  "pin_state_chooch": "on", "pin_state_dechooch": "off" }


'''
list of motors is a list of devices that are 'motors', or things that we want to 
ensure are in an 'off' state if the app crashes and restarts suddenly
this is a list of dicts, not strings
'''

LIST_OF_MOTORS = [BARREL_MOTOR, VIBRATION_MOTOR]


'''
instantiate some global things that get passed around
'''
global counter
global pin_input_settings
global pin_out_settings


counter = 0
pin_input_settings = {}
pin_out_settings = {}
PICS_DIR = "/home/pi/pics"
