import RPi.GPIO as GPIO
import time
import random
import urllib3
from Config import *
from Camera import *

### Pin control stuff ###
def pin_input(pin_number,callback,bouncetime=2000):
    '''
    Takes an int pin number and the name of a callback function as args
    This assumes wiring pi numbering. 
    Will return true if GPIO doesn't throw an exception.
    Sets up rising voltage detection with activation of the board's internal resistor. 
    Still recommended to have other resistor before the input actuator. 
    bouncetime is an integer in milliseconds that the input detection will wait before accepting another input
    '''
    pin_number = int(pin_number)
    if pin_number in pin_input_settings and callback not in pin_input_settings[pin_number]:
        GPIO.add_event_callback(pin_number, lambda x: globals()[callback]())
        pin_input_settings[pin_number].append(callback)
        return { "status" : 1, "message" : "added callback %s, pin %d" % (callback, pin_number), "current_settings" : pin_input_settings }
    try:
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin_number, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(pin_number, GPIO.RISING, callback = lambda x: globals()[callback](), bouncetime=bouncetime)
        pin_input_settings[pin_number] = [callback]
    except RuntimeError:
        state = {"status" :0 , "message": "Runtime error when trying to set up pin. Try restarting this server" }
    state = {"status": 1, "message": "pin %d set up to listen for rising voltage and execute %s" %(pin_number,callback), "current_settings": pin_input_settings }
    return state

def pin_callback_removal(pin_number):
    '''
    returns true, wipes pin callbacks for specified pin
    '''
    global pin_input_settings
    pin_input_settings[pin_number] = False
    if not pin_number in pin_input_settings:
        return { "status" : 0, "message" : "pin number not found in current dict. if this is wrong, restart the app" }
    try:
        pin_number = int(pin_number)
    except ValueError:
        state = { "status" : 0 , "message" :"not a number, bro"}
        return state
    GPIO.setmode(GPIO.BOARD)
    GPIO.remove_event_detect(pin_number)
    state = { "status": 1, "message" : "pin %d unassigned all callbacks" % (pin_number) }
    return state

def pin_output(number,action):
    '''
    turns a pin on or off
    number is an int of the pin on the board. this assumes wiringpi numbering
    status key in return dict will return 0 if nothing was could be done with the pin
    '''
    global pin_out_settings
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    
    try:
        number = int(number)
    except ValueError:
        state = {"status": 0 , "message": "invalid pin number" }
        return state
   
    try:
        GPIO.setup(number,GPIO.OUT)
    except Exception as e:
        state = {"status": 0, "message": "Invalid pin for activation"}
        return state
       
    if action in ["on","off"]:
        if action == "on":
            GPIO.output(number, True)
            state = { "status": 1, "message" : "pin %d on" % (number) }
            pin_out_settings[number] = action
        elif action == "off":
            GPIO.output(number, 0)
            state = {"status": 1, "message" : "pin %d off" % (number) }
            pin_out_settings[number] = action
    else:
        state = {"status": 0, "message": "invalid action"}
    return state

def toggle_pin_output(number):
    global pin_out_settings
    try:
        number = int(number)
    except ValueError:
        state = {"status": 0 , "message": "invalid pin number" }
        return state
    if number not in pin_out_settings:
        return { "status" : 0 , "message" : "pin not registered" }

    current_pin_state = pin_out_settings[number]
    if current_pin_state == "on":
        pin_output(number,"off")
    if current_pin_state == "off":
        pin_output(number,"on")
    return { "status" : "1", "message" : "toggled", "pin" : pin_out_settings[number] }
     
### end pin control stuff ###


### Callbacks, no input, no returns, just does shit ###
def blink():
    pin_output(37,"on")
    time.sleep(5)
    pin_output(37,"off")


def global_counter_inc():
    global counter
    counter = counter + 1

def get_counter():
    global counter
    return counter

def reset_counter():
    global counter
    del counter  
    counter = 0


def vibration_cycle():
    pin_output(VIBRATION_MOTOR["pin_number"], VIBRATION_MOTOR["pin_state_chooch"])
    time.sleep(random.randint(1,3))
    pin_output(VIBRATION_MOTOR["pin_number"], VIBRATION_MOTOR["pin_state_dechooch"])

#### callbacks end ####



### doing stuff. can be used from view callables ###
def feeding(feedsize=1):
    '''
    feed_size is an int, which is the number of times the
    barrel rotates and drops food in down the tray
    when the barrel rotates, it eventually causes a switch
    to close, sending input to a pin, which will increment a 
    counter. when counter reaches feed_size, the barrel motor
    is stopped
    '''
    global counter
    reset_counter()
    try:
        feedsize = int(feedsize)
    except ValueError:
        return { "status" : 0, "message":"not an number" }

    if feedsize > 0 and feedsize < 6:
        pin_input(ROTATION_SENSOR["pin_number"], "global_counter_inc", bouncetime=2000)
        pin_input(ROTATION_SENSOR["pin_number"], "vibration_cycle")
        vibration_cycle()
        pin_output(BARREL_MOTOR["pin_number"], BARREL_MOTOR["pin_state_chooch"])
        while True:
            if counter > feedsize:
                time.sleep(10)
                break
        pin_output(BARREL_MOTOR["pin_number"], BARREL_MOTOR["pin_state_dechooch"])        
        reset_counter()
        return { "status" : 1, "message" : "feeding done", "feedsize" : feedsize }
    else:
        return { "status" : 0, "message" : "feed size must be 5 or less" }

def motion_toggle():
    toggle_pin_output(40)
### doing stuff end ###


def dechooch_motors():
    log  = []
    for motor in LIST_OF_MOTORS:
        log.append(pin_output(motor["pin_number"], motor["pin_state_dechooch"]))
    return { "status" : "1", "message" : "configured motors turned off", "motors" : LIST_OF_MOTORS, "log": log }

