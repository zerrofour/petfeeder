#!/bin/bash

sudo apt-get update
sudo apt-get install nginx supervisor vim


cd ~/petfeeder

sudo cp ~/petfeeder/configs/nginx.conf /etc/nginx/
sudo cp ~/petfeeder/configs/flask.conf /etc/nginx/conf.d/
sudo cp ~/petfeeder/configs/supervisor_flask.conf /etc/supervisor/conf.d/

systemctl enable nginx
systemctl enable supervisor
